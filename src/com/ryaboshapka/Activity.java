package com.ryaboshapka;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Activity {
    public List<SalariedEmployee> getSalariedEmployee() {
        ArrayList<SalariedEmployee> salariedList = new ArrayList<>();
        salariedList.add(new SalariedEmployee(25,"Egor","Ryaboshapka","University", 1,(int)(Math.random()*77777)+100000,true));
        salariedList.add(new SalariedEmployee(20,"Vika","Kirikova","University", 2,(int)(Math.random()*77777)+100000,false));
        salariedList.add(new SalariedEmployee(21,"Anton","Lavrinyk","University", 3,(int)(Math.random()*77777)+100000,true));
        salariedList.add(new SalariedEmployee(35,"Dima","Palchin","University", 4,(int)(Math.random()*77777)+100000,false));
        return salariedList;
    }
    public List<ContractEmployee> getContractEmployee() {
        ArrayList<ContractEmployee> contractList = new ArrayList<>();
        contractList.add(new ContractEmployee(24,"Karina","Fedorova","University", 5,(int)(Math.random()*6666)+100000,true));
        contractList.add(new ContractEmployee(25,"Marina","Kuzhez","University", 6,(int)(Math.random()*6666)+100000,false));
        contractList.add(new ContractEmployee(27,"Darina","Hunchun","University", 7,(int)(Math.random()*6666)+100000,true));
        contractList.add(new ContractEmployee(32,"Katya","Korchun","University", 8,(int)(Math.random()*6666)+100000,true));
        return contractList;
    }
    public void calculatePay(List<SalariedEmployee> salariedEmployees,List<ContractEmployee> contractEmployees){
        ContractEmployee contractEmployee;
        for(int i = 0; i < contractEmployees.size();i++){
            contractEmployee  = contractEmployees.get(i);
            System.out.println("Contract Employee: " + contractEmployee.getFirstName() + " with Federal Tax ID member: "+ contractEmployee.getFederalTaxIDmember() + " get paid every week");
            System.out.println("Salary " + calculateSallaryForContractEmployee(contractEmployee));
        }
        System.out.println();
        SalariedEmployee salariedEmployee;
        for(int i = 0; i < salariedEmployees.size();i++){
            salariedEmployee = salariedEmployees.get(i);
            System.out.println("Salaried Employee: " + salariedEmployee.getFirstName() + " with Social Security Number: " + salariedEmployee.getSocialSecurityNumber() + " get paid every month");
            System.out.println("Salary " + calculateSallaryForSalariedEmployee(salariedEmployee));
        }
    }
    public int calculateSallaryForContractEmployee(Employee employee){
        double rangeMin = 100;
        double rangeMax = 500;
        double coefIfHasExperience = employee.isHasExperience()? 1.2:1;
        Random r = new Random();
        double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        int answer = (int) Math.round(employee.getAge() * randomValue * coefIfHasExperience);
       return answer;
    }
    public int calculateSallaryForSalariedEmployee(Employee employee){
        double rangeMin = 300;
        double rangeMax = 600;
        double coefIfHasExperience = employee.isHasExperience()? 1.6:1;
        Random r = new Random();
        double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        int answer = (int) Math.round(employee.getAge() * randomValue * coefIfHasExperience);
        return answer;
    }
}

package com.ryaboshapka;
public class SalariedEmployee extends Employee{
    private final int socialSecurityNumber;
    public SalariedEmployee(int age, String firstName, String lastName,String education, int employeeID,int socialSecurityNumber, boolean hasExperience) {
        super(age, firstName, lastName,education, employeeID, hasExperience);
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public int getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
}

package com.ryaboshapka;

public class Employee {
    private int age;
    private String firstName;
    private String lastName;
    private String education;
    private boolean hasExperience;
    private int employeeID;

    public Employee(int age, String firstName, String lastName,String education, int employeeID, boolean hasExperience){
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.education = education;
        this.employeeID = employeeID;
        this.hasExperience = hasExperience;
    }

    public int getAge() {
        return age;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public String getEducation() {
        return education;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isHasExperience() {
        return hasExperience;
    }
}

package com.ryaboshapka;
public class ContractEmployee extends Employee{
    private final int federalTaxIDmember;
    public ContractEmployee(int age, String firstName, String lastName,String education, int employeeID,int federalTaxIDmember, boolean hasExperience) {
        super(age, firstName, lastName,education, employeeID, hasExperience);
        this.federalTaxIDmember = federalTaxIDmember;
    }

    public int getFederalTaxIDmember() {
        return federalTaxIDmember;
    }
}
